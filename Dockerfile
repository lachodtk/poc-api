FROM python:3.6-alpine 

RUN mkdir /app
COPY . /app
WORKDIR /app
RUN pip install -r requirements/requirements.txt
EXPOSE 8081

USER 0
CMD [ "python", "main.py" ]
