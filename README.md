### Configuration

You will need to set the next environment variables:

| Variable   | Value                      |
| :--------: | :------------------------: |
| DB_URL     | The database complete url  |


### Development installation

Install the dependencies and start the server:

```sh
$ pip3 install -r requirements.dev.txt
$ pip3 install -r requirements.txt
$ python3 main.py
```

### Development notes

Generate `requirements.txt` with:

```sh
$ pipreqs . --savepath ./requirements/requirements.txt
```

Code linting is performed with:

```sh
$ find . -iname "*.py" |xargs pylint
```

Tests can be run with:

```sh
$ python3 -m unittest discover -v . "*_test.py"
```
