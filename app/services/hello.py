from flask import jsonify
from flask.views import MethodView


class HelloAPI(MethodView):

    def get(self):
        return jsonify({'data': 'hello Devsu!!!'})
