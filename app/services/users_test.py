import unittest
from app.services.hello import UserAPI


class UserAPITest(unittest.TestCase):

    def test_should_return_11(self):
        api = UserAPI()
        actual = api.get()
        expected = '11'
        self.assertEqual(actual, expected)


if __name__ == '__main__':
    unittest.main()
